const plugin = require('tailwindcss/plugin')
const { backgroundColor } = require('tailwindcss/defaultTheme')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './app/helpers/**/*.rb',
    './app/javascript/**/*.js',
    './app/views/**/*.{erb,haml,html,slim}',
    './app/components/**/*.{erb,rb,html,slim}'
  ],
  theme: {
    colors: {
      'blue-1': '#345BFF',
      'blue-2': '#A4B4F5',
      'blue-3': '#E5EAFF',
      'blue-4': '#EFF2FF',
      'gray-1': '#272727',
      'gray-2': '#696969',
      'gray-3': '#A9A9A9',
      'gray-4': '#D4D4D4',
      'gray-5': '#DEDEDE',
      'gray-6': '#EFEFEF',
      'gray-7': '#F6F6F6',
      'red': '#FF344C',
      'white': "#FFF",
      'black': "#000",
    },
    extend: {
      fontFamily: {
        sans: ['Source Sans Pro', 'sans-serif'],
      },
      backgroundImage: {
        'elements': "url('/assets/bg-element-right.svg'), url('/assets/bg-element-left.svg')",
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),
    plugin(function({ addComponents }) {
      addComponents({
        '.bow': {
          'border-radius': '0% 100% 100% 0% / 53% 48% 52% 47%',
        },
        '.w-88':{
          'width': '22rem',
        },
        '.transparent-bg': {
          'background-color': 'transparent',
        },
        '.writing-vertical':{
          'writing-mode': 'vertical-rl',
        }, 
        '.bg-elements':{
          'background-position': '100% 50%, 0 0',
          'background-attachment': 'fixed, fixed',
          'background-repeat': 'no-repeat, no-repeat',
        },
        '.nav-bg':{
          'background': 'radial-gradient(100% 100% at 50% 0%, #FFFFFF 0%, #F6F6F6 100%)',
        }
      })
    })
  ],
}
