# frozen_string_literal: true

class ShipComponent < ViewComponent::Base
  def initialize(type:)
    @type = type
  end

  def ship_style
    case @type
    when 'aircraft'
      'rounded-l'
    when 'submarine'
      'rounded-full'
    when 'destroyer'
      'rounded-full'
    when 'battleship'
      'rounded-full'
    end
  end
end
