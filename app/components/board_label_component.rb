# frozen_string_literal: true

class BoardLabelComponent < ViewComponent::Base
  def initialize(title:)
    @title = title
  end

  def bg_color
    case @title
    when 'my-fleet'
      'bg-blue-1'
    when 'opponent-fleet'
      'bg-gray-3'
    end
  end
end
