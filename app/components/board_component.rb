# frozen_string_literal: true

class BoardComponent < ViewComponent::Base
  def initialize(board:)
    @board = board
  end

  def bg_color
    bg_colors = {}
    case @board
    when 'my-board'
      default = "bg-blue-4"
      hover = "hover:bg-blue-3"
      checked = "checked:bg-blue-2"
    when 'opponent-board'
      default = "bg-gray-6"
      hover = "hover:bg-gray-5"
      checked = "checked:bg-gray-4"
    end
    bg_colors[:default] = default
    bg_colors[:hover] = hover
    bg_colors[:checked] = checked
    return bg_colors
  end
end
