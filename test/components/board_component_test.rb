# frozen_string_literal: true

require "test_helper"

class BoardComponentTest < ViewComponent::TestCase
  def test_component_renders_my_board_label
    assert_selector("div[title='my-fleet']", text: "Your Fleet")
  end

  def test_component_renders_opponent_board_label
    assert_selector("div[title='opponent-fleet']", text: "Opponent")
  end
end
